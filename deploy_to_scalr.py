import sys
import datetime
import urllib
import base64
import hmac
import hashlib

API_URL = 'http://52.53.251.240/api/api.php'
API_VERSION = '2.3.0'
API_AUTH_VERSION =  '3'

FARM_ID = 2

def get_server_id(key_id, secret_key):
    timestamp = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

    params = {
        "Action": "FarmGetDetails",
        "FarmID": FARM_ID,
        "Version": API_VERSION,
        "AuthVersion": API_AUTH_VERSION,
        "Timestamp": timestamp,
        "KeyID": key_id,
        "Signature":  base64.b64encode(hmac.new(secret_key, ":".join(["FarmGetDetails", key_id, timestamp]), hashlib.sha256).digest()),
    }

    urlparams = urllib.urlencode(params)
    req = urllib.urlopen(API_URL, urlparams)

    resp = req.read()
    server_id = resp[resp.find('<ServerID>') + len('<ServerID>'):resp.find('</ServerID>')];
    print resp
    print "Extracted id:", server_id
    return server_id

def main(key_id, secret_key):
    server_id = get_server_id(key_id, secret_key)

    timestamp = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")

    params = {
        "Action": "FireCustomEvent",
        "EventName": "DeploymentNeeded",
        "ServerID": server_id,
        "Version": API_VERSION,
        "AuthVersion": API_AUTH_VERSION,
        "Timestamp": timestamp,
        "KeyID": key_id,
        "Signature":  base64.b64encode(hmac.new(secret_key, ":".join(["FireCustomEvent", key_id, timestamp]), hashlib.sha256).digest()),
    }

    urlparams = urllib.urlencode(params)
    req = urllib.urlopen(API_URL, urlparams)

    return req.read()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: {} api_key api_secret'.format(sys.argv[0]))
        sys.exit(1)
    api_key = sys.argv[1]
    api_secret = sys.argv[2]
    print main(api_key, api_secret)

        

